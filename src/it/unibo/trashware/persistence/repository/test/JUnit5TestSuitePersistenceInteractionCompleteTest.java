package it.unibo.trashware.persistence.repository.test;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

/**
 * A JUnit Suite test which tests all the CRUD operations on all the entities in the system.
 * 
 * @author Manuel Bonarrigo
 *
 */
@RunWith(JUnitPlatform.class)
@SelectPackages({"it.unibo.trashware.persistence.repository.test.adapter",
                 "it.unibo.trashware.persistence.repository.test.mapper",
                 "it.unibo.trashware.persistence.repository.test.metamapping",
                 "it.unibo.trashware.persistence.repository.test.query",
                 "it.unibo.trashware.persistence.repository.test.security",
                 "it.unibo.trashware.persistence.repository.test.utils"})
public class JUnit5TestSuitePersistenceInteractionCompleteTest { }

