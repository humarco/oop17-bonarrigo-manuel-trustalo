package it.unibo.trashware.persistence.repository.crud.domain;

import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentDeviceCategory;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentDeviceWorkProgress;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentGenericDevice;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentLegalCategoryCompound;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentRefinedDevice;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentRefinedDeviceCompound;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentRefinedDeviceCompoundWithGeneric;
import it.unibo.trashware.persistence.repository.crud.entities.devices.management.PersistentVendor;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentAspectRatio;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentCase;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentColor;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentDigitalInformationUnit;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentHardDiskDrive;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentPrinter;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentPrinterCategory;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentRandomAccessMemory;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentScreen;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentScreenCategory;
import it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy.PersistentScreenResolution;

/**
 * A marker interface constituted by the extension of all the interfaces which
 * semantically compose the Devices domain.
 * <p>
 * The implementor of this class <i> should </i> have direct access to the
 * persistence media, as the returning values are expected to be the result of a
 * data query.
 * 
 * @author Manuel Bonarrigo
 */
public interface CrudDevices extends PersistentAspectRatio, PersistentCase, PersistentColor, PersistentDeviceCategory,
        PersistentDeviceWorkProgress, PersistentDigitalInformationUnit, PersistentGenericDevice,
        PersistentHardDiskDrive, PersistentLegalCategoryCompound, PersistentPrinter, PersistentPrinterCategory,
        PersistentRandomAccessMemory, PersistentRefinedDeviceCompound, PersistentRefinedDeviceCompoundWithGeneric,
        PersistentScreen, PersistentScreenCategory, PersistentScreenResolution, PersistentVendor,
        PersistentRefinedDevice {

}
