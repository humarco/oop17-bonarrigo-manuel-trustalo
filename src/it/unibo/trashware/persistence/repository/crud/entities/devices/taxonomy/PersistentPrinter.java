package it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy;

import java.util.Set;

import it.unibo.trashware.persistence.model.devices.Printer;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link Printer}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentPrinter {
    /**
     * The CRUD operation of proposing a new {@link Printer} to be created.
     * 
     * @param printer
     *            the Printer to be created.
     * @throws DuplicateKeyValueException
     *             if the Printer to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(Printer printer) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link Printer} filtered by
     * the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the Printer objects matched against the filter
     */
    Set<Printer> readPrinter(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link Printer} to be updated with the
     * value of a new one.
     * 
     * @param oldPrinter
     *            the Printer actually stored.
     * @param newPrinter
     *            the Printer with the informations to be fetched for update.
     * @throws DuplicateKeyValueException
     *             if the Printer update transforms the entity in an already present
     *             one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(Printer oldPrinter, Printer newPrinter) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link Printer} for deletion.
     * 
     * @param printer
     *            the Printer to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(Printer printer) throws NonExistentReferenceException, BoundedReferenceException;

}
