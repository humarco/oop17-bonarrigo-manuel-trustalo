/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Indexes;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Keys;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Trustalo;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records.PrintertecnologiesRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Printertecnologies extends TableImpl<PrintertecnologiesRecord> {

    private static final long serialVersionUID = -791636138;

    /**
     * The reference instance of <code>trustalo.printertecnologies</code>
     */
    public static final Printertecnologies PRINTERTECNOLOGIES = new Printertecnologies();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PrintertecnologiesRecord> getRecordType() {
        return PrintertecnologiesRecord.class;
    }

    /**
     * The column <code>trustalo.printertecnologies.ID</code>.
     */
    public final TableField<PrintertecnologiesRecord, UByte> ID = createField("ID", org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>trustalo.printertecnologies.Name</code>.
     */
    public final TableField<PrintertecnologiesRecord, String> NAME = createField("Name", org.jooq.impl.SQLDataType.VARCHAR(30).nullable(false), this, "");

    /**
     * Create a <code>trustalo.printertecnologies</code> table reference
     */
    public Printertecnologies() {
        this(DSL.name("printertecnologies"), null);
    }

    /**
     * Create an aliased <code>trustalo.printertecnologies</code> table reference
     */
    public Printertecnologies(String alias) {
        this(DSL.name(alias), PRINTERTECNOLOGIES);
    }

    /**
     * Create an aliased <code>trustalo.printertecnologies</code> table reference
     */
    public Printertecnologies(Name alias) {
        this(alias, PRINTERTECNOLOGIES);
    }

    private Printertecnologies(Name alias, Table<PrintertecnologiesRecord> aliased) {
        this(alias, aliased, null);
    }

    private Printertecnologies(Name alias, Table<PrintertecnologiesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Trustalo.TRUSTALO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PRINTERTECNOLOGIES_NAME, Indexes.PRINTERTECNOLOGIES_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<PrintertecnologiesRecord, UByte> getIdentity() {
        return Keys.IDENTITY_PRINTERTECNOLOGIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PrintertecnologiesRecord> getPrimaryKey() {
        return Keys.KEY_PRINTERTECNOLOGIES_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PrintertecnologiesRecord>> getKeys() {
        return Arrays.<UniqueKey<PrintertecnologiesRecord>>asList(Keys.KEY_PRINTERTECNOLOGIES_PRIMARY, Keys.KEY_PRINTERTECNOLOGIES_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Printertecnologies as(String alias) {
        return new Printertecnologies(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Printertecnologies as(Name alias) {
        return new Printertecnologies(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Printertecnologies rename(String name) {
        return new Printertecnologies(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Printertecnologies rename(Name name) {
        return new Printertecnologies(name, null);
    }
}
