/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records;


import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.Devicerequestsdeviceswithid;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DevicerequestsdeviceswithidRecord extends UpdatableRecordImpl<DevicerequestsdeviceswithidRecord> implements Record2<UInteger, UInteger> {

    private static final long serialVersionUID = -1339416631;

    /**
     * Setter for <code>trustalo.devicerequestsdeviceswithid.RequestDevice</code>.
     */
    public void setRequestdevice(UInteger value) {
        set(0, value);
    }

    /**
     * Getter for <code>trustalo.devicerequestsdeviceswithid.RequestDevice</code>.
     */
    public UInteger getRequestdevice() {
        return (UInteger) get(0);
    }

    /**
     * Setter for <code>trustalo.devicerequestsdeviceswithid.DeviceWithID</code>.
     */
    public void setDevicewithid(UInteger value) {
        set(1, value);
    }

    /**
     * Getter for <code>trustalo.devicerequestsdeviceswithid.DeviceWithID</code>.
     */
    public UInteger getDevicewithid() {
        return (UInteger) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record2<UInteger, UInteger> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<UInteger, UInteger> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<UInteger, UInteger> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field1() {
        return Devicerequestsdeviceswithid.DEVICEREQUESTSDEVICESWITHID.REQUESTDEVICE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field2() {
        return Devicerequestsdeviceswithid.DEVICEREQUESTSDEVICESWITHID.DEVICEWITHID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger component1() {
        return getRequestdevice();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger component2() {
        return getDevicewithid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value1() {
        return getRequestdevice();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value2() {
        return getDevicewithid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DevicerequestsdeviceswithidRecord value1(UInteger value) {
        setRequestdevice(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DevicerequestsdeviceswithidRecord value2(UInteger value) {
        setDevicewithid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DevicerequestsdeviceswithidRecord values(UInteger value1, UInteger value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached DevicerequestsdeviceswithidRecord
     */
    public DevicerequestsdeviceswithidRecord() {
        super(Devicerequestsdeviceswithid.DEVICEREQUESTSDEVICESWITHID);
    }

    /**
     * Create a detached, initialised DevicerequestsdeviceswithidRecord
     */
    public DevicerequestsdeviceswithidRecord(UInteger requestdevice, UInteger devicewithid) {
        super(Devicerequestsdeviceswithid.DEVICEREQUESTSDEVICESWITHID);

        set(0, requestdevice);
        set(1, devicewithid);
    }
}
