/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Indexes;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Keys;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Trustalo;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records.AspectratiosRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Aspectratios extends TableImpl<AspectratiosRecord> {

    private static final long serialVersionUID = -260465143;

    /**
     * The reference instance of <code>trustalo.aspectratios</code>
     */
    public static final Aspectratios ASPECTRATIOS = new Aspectratios();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AspectratiosRecord> getRecordType() {
        return AspectratiosRecord.class;
    }

    /**
     * The column <code>trustalo.aspectratios.ID</code>.
     */
    public final TableField<AspectratiosRecord, UByte> ID = createField("ID", org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>trustalo.aspectratios.RatioFactor</code>.
     */
    public final TableField<AspectratiosRecord, String> RATIOFACTOR = createField("RatioFactor", org.jooq.impl.SQLDataType.VARCHAR(5).nullable(false), this, "");

    /**
     * Create a <code>trustalo.aspectratios</code> table reference
     */
    public Aspectratios() {
        this(DSL.name("aspectratios"), null);
    }

    /**
     * Create an aliased <code>trustalo.aspectratios</code> table reference
     */
    public Aspectratios(String alias) {
        this(DSL.name(alias), ASPECTRATIOS);
    }

    /**
     * Create an aliased <code>trustalo.aspectratios</code> table reference
     */
    public Aspectratios(Name alias) {
        this(alias, ASPECTRATIOS);
    }

    private Aspectratios(Name alias, Table<AspectratiosRecord> aliased) {
        this(alias, aliased, null);
    }

    private Aspectratios(Name alias, Table<AspectratiosRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Trustalo.TRUSTALO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ASPECTRATIOS_PRIMARY, Indexes.ASPECTRATIOS_RATIOFACTOR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<AspectratiosRecord, UByte> getIdentity() {
        return Keys.IDENTITY_ASPECTRATIOS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<AspectratiosRecord> getPrimaryKey() {
        return Keys.KEY_ASPECTRATIOS_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<AspectratiosRecord>> getKeys() {
        return Arrays.<UniqueKey<AspectratiosRecord>>asList(Keys.KEY_ASPECTRATIOS_PRIMARY, Keys.KEY_ASPECTRATIOS_RATIOFACTOR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Aspectratios as(String alias) {
        return new Aspectratios(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Aspectratios as(Name alias) {
        return new Aspectratios(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Aspectratios rename(String name) {
        return new Aspectratios(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Aspectratios rename(Name name) {
        return new Aspectratios(name, null);
    }
}
